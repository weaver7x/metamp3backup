package launch;

import java.io.File;
import org.apache.catalina.startup.Tomcat;

/**
 * Clase principal que lanza la aplicación MetaMP3Backup con un Apache Tomcat
 * embebido.
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public final class Main {

    /**
     * Lanza la aplicación en el puerto 8080 y en "src/main/webapp/" como
     * directorio base.
     *
     * @param args Parámetros de entrada.
     * @throws Exception Si el servidor no puede ser lanzado.
     */
    public static void main(final String[] args) throws Exception {
        final Tomcat tomcat = new Tomcat();
        tomcat.setPort(8080);
        tomcat.addWebapp("/", new File("src/main/webapp/").getAbsolutePath());
        /* Debug*/
        //System.out.println("Configurando la aplicación en: " + new File("./" + webappDirLocation).getAbsolutePath());
        //System.out.println("Puerto: " + ((webPort == null || webPort.isEmpty()) ? "8080" : webPort));
        tomcat.start();
        tomcat.getServer().await();
    }
}