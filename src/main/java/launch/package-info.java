/**
 * <p>
 * Contiene las clases encargadas del lanzamiento de la aplicación.
 * </p>
 *
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
package launch;