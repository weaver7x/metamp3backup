package metamp3backup.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import metamp3backup.controller.AbstractController;
import metamp3backup.controller.ControllerException;
import metamp3backup.controller.EdicionController;
import metamp3backup.controller.IController;
import org.codehaus.jettison.json.JSONObject;

/**
 * Modificar los metadatos de los ficheros (físicamente): /Edicion
 *
 * Parámetros:
 *
 * token: token de autenticación obtenido.
 *
 * id: id de la pista a editar.
 *
 * [opcional] nombre: nombre de la pista editado encoded.
 *
 * [opcional] artista: artista de la pista editado encoded.
 *
 * [opcional] genero: id del género de la pista encoded.
 *
 * POST (ejemplo)
 *
 * /Edicion?token=asb12ka81_1&id=1212&nombre="nombreEncoded"&artista="artistaEncoded"&genero=12
 *
 * Respuesta (ejemplo edición correcta): éxito o fallo en la operación.
 *
 * {
 *
 * "estado":"exito"
 *
 * }
 *
 * Respuesta (ejemplo edición fallida)
 *
 * {
 *
 * "estado":"fallo"
 *
 * }
 *
 * Respuesta (ejemplo no identificado)
 *
 * {
 *
 * "estado":"nologin"
 *
 * }
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public final class Edicion extends HttpServlet {

    protected void processRequest(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {

        try {
            request.setCharacterEncoding("UTF-8");
            final IController mController = new EdicionController();
            mController.init(request);
            mController.execute();

            response.setContentType("application/json;charset=UTF-8");
            final PrintWriter out = response.getWriter();
            final JSONObject objetoJSON = (JSONObject) request.getAttribute(AbstractController.OBJETO_JSON);
            out.print(objetoJSON != null ? objetoJSON : "");
            out.flush();
        } catch (final ControllerException ce) {
            System.out.println("Error en Edición\n" + ce.getMessage());
        }
    }

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}