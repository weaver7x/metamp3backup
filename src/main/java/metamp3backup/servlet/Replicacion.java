package metamp3backup.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import metamp3backup.controller.AbstractController;
import metamp3backup.controller.ControllerException;
import metamp3backup.controller.IController;
import metamp3backup.controller.ReplicacionController;
import org.codehaus.jettison.json.JSONObject;

/**
 * Indentificación del servidor central: /Replicacion
 *
 * Parámetros:
 *
 * token: token de autenticación obtenido.
 *
 * id: id de la pista a replicar.
 *
 * zona: zona del destino de la replicación.
 *
 * POST (ejemplo)
 *
 * /Replicacion?token=asb12ka81_1&id=1212&zona=2
 *
 * Respuesta (ejemplo subida correcta): exito o fallo en la operación.
 *
 * {
 *
 * "estado":"exito"
 *
 * }
 *
 * Respuesta (ejemplo subida fallida)
 *
 * {
 *
 * "estado":"fallo"
 *
 * }
 *
 * Respuesta (ejemplo no identificado)
 *
 * {
 *
 * "estado":"nologin"
 *
* }
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public final class Replicacion extends HttpServlet {

    protected void processRequest(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {

        try {
            request.setCharacterEncoding("UTF-8");
            final IController mController = new ReplicacionController();
            mController.init(request);
            mController.execute();

            response.setContentType("application/json;charset=UTF-8");
            final PrintWriter out = response.getWriter();
            final JSONObject objetoJSON = (JSONObject) request.getAttribute(AbstractController.OBJETO_JSON);
            out.print(objetoJSON != null ? objetoJSON : "");
            out.flush();
        } catch (final ControllerException ce) {
            System.out.println("Error en Replicacion\n" + ce.getMessage());
        }
    }

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}