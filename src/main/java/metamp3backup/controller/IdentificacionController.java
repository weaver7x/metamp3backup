package metamp3backup.controller;

import static metamp3backup.controller.AbstractController.OBJETO_JSON;
import org.apache.catalina.util.SessionIdGenerator;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * Controlador del metodo de la API del servidor /Identificacion
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public final class IdentificacionController extends AbstractController {

    @Override
    public void execute() throws ControllerException {
        final String idJSON = this.request.getParameter(ConstantsJSON.BACKUP_API_KEY_ID);
        final String passJSON = this.request.getParameter(ConstantsJSON.BACKUP_API_KEY_PASS);
        JSONObject respuestaJSON = new JSONObject();
        if (idJSON != null && passJSON != null) {
            System.out.println("/Identificacion - Identificación de servidor0");
            try {
                // Seguridad de la aplicación.
                // Como es un proyecto de juguete se hace así :)
                // Usuario: "1",MD5 = c4ca4238a0b923820dcc509a6f75849b
                // Contraseña: "servidor0",MD5 =15fddd714b02884f340bb2304e1b43d4
                respuestaJSON = new JSONObject();
                if (Hash.md5(idJSON).equals(Hash.md5("1")) && Hash.md5(passJSON).equals(Hash.md5("servidor0"))) {
                    respuestaJSON.put(ConstantsJSON.BACKUP_API_KEY_ESTADO, ConstantsJSON.BACKUP_API_KEY_ESTADO_EXITO);
                    respuestaJSON.put(ConstantsJSON.BACKUP_API_KEY_TOKEN, new SessionIdGenerator().generateSessionId());
                } else {
                    respuestaJSON.put(ConstantsJSON.BACKUP_API_KEY_ESTADO, ConstantsJSON.BACKUP_API_KEY_ESTADO_FALLO);
                }
                this.request.setAttribute(OBJETO_JSON, respuestaJSON);
            } catch (final JSONException ex) {
                ex.printStackTrace();
                respuestaJSON = new JSONObject();
                try {
                    respuestaJSON.put(ConstantsJSON.BACKUP_API_KEY_ESTADO, ConstantsJSON.BACKUP_API_KEY_ESTADO_FALLO);
                } catch (final JSONException ex1) {
                }
            }
        } else {
            this.request.setAttribute(OBJETO_JSON, null);
        }
    }
}