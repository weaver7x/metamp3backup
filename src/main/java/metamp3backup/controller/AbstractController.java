package metamp3backup.controller;

import javax.servlet.http.HttpServletRequest;

/**
 * Controlador abstracto a seguir por todos los controladores de la aplicación.
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public abstract class AbstractController implements IController {

    /**
     * Objeto JSON que será devuelto en la llamada.
     */
    public final static String OBJETO_JSON = "objetoJSON";
    /**
     * Request object.
     */
    protected HttpServletRequest request;

    @Override
    public final void init(final HttpServletRequest request) {
        this.request = request;
    }
}