package metamp3backup.controller;

/**
 * Genera encriptaciones MD5 y SHA1.
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 1.0-FINAL
 */
public final class Hash {

    /**
     * Obtiene la encriptación Hash de un String.
     *
     * @param txt Text in plain format..
     * @param hashType MD5 OR SHA1
     * @return Hash in hashType.
     */
    public static String getHash(final String txt, final String hashType) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance(hashType);
            final byte[] array = md.digest(txt.getBytes());
            final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            //error action
        }
        return null;
    }

    public static String md5(final String txt) {
        return Hash.getHash(txt, "MD5");
    }

    public static String sha1(final String txt) {
        return Hash.getHash(txt, "SHA1");
    }
}