package metamp3backup.controller;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * Controlador del metodo de la API del servidor /Edicion
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public final class EdicionController extends AbstractController {

    @Override
    public void execute() throws ControllerException {
        final String tokenJSON = this.request.getParameter(ConstantsJSON.BACKUP_API_KEY_TOKEN);
        final String idPistaJSON = this.request.getParameter(ConstantsJSON.BACKUP_API_KEY_PISTA_ID);
        // Parámetros opcionales
        final String nombreCancionPistaJSON = this.request.getParameter(ConstantsJSON.BACKUP_API_KEY_PISTA_NOMBRE_CANCION);
        final String artistaPistaJSON = this.request.getParameter(ConstantsJSON.BACKUP_API_KEY_PISTA_ARTISTA);
        final String idGeneroPistaJSON = this.request.getParameter(ConstantsJSON.BACKUP_API_KEY_PISTA_ID_GENERO);
        // Fin parámetros opcionales
        JSONObject respuestaJSON;
        if (tokenJSON != null && idPistaJSON != null) {
            System.out.println("/Edicion\n" + idPistaJSON + ' ' + nombreCancionPistaJSON + ' ' + artistaPistaJSON + ' ' + idGeneroPistaJSON);
            respuestaJSON = new JSONObject();
            try {
                final int idPista = Integer.parseInt(idPistaJSON);

                // Seguridad de la aplicación: tokenJSON se da por supuesto que sí.
                if (true) {
                    System.out.println("\t **Gestor de recuperación -> Edición: prepared.**");
                    System.out.println("\t - Edito fichero: " + ConstantsJSON.RUTA_INTERNA_WEBDAV + idPista + ConstantsJSON.EXTENSION_FICHEROS_CON_PUNTO);
                    System.out.println("\t Edito fichero: " + ConstantsJSON.RUTA_INTERNA_WEBDAV + idPista + ConstantsJSON.EXTENSION_FICHEROS_CON_PUNTO);
                    if (new AudioTagger().updateFichero(ConstantsJSON.RUTA_INTERNA_WEBDAV + idPista + ConstantsJSON.EXTENSION_FICHEROS_CON_PUNTO, nombreCancionPistaJSON, artistaPistaJSON, idGeneroPistaJSON) == AudioTagger.EDICION_CORRECTA) {
                        respuestaJSON.put(ConstantsJSON.BACKUP_API_KEY_ESTADO, ConstantsJSON.BACKUP_API_KEY_ESTADO_EXITO);
                        System.out.println("\t **Gestor de recuperación -> Edición: committed.**");
                    } else {
                        respuestaJSON.put(ConstantsJSON.BACKUP_API_KEY_ESTADO, ConstantsJSON.BACKUP_API_KEY_ESTADO_FALLO);
                        System.out.println("\t **Gestor de recuperación -> Edición: aborted -> Intentar de nuevo más tarde.**");
                    }

                } else {
                    respuestaJSON.put(ConstantsJSON.BACKUP_API_KEY_ESTADO, ConstantsJSON.BACKUP_API_KEY_ESTADO_FALLO_NO_LOGIN);
                }
                this.request.setAttribute(OBJETO_JSON, respuestaJSON);
            } catch (final JSONException ex) {
                ex.printStackTrace();
                try {
                    respuestaJSON.put(ConstantsJSON.BACKUP_API_KEY_ESTADO, ConstantsJSON.BACKUP_API_KEY_ESTADO_FALLO);
                } catch (final JSONException ex1) {
                }
            }
        } else {
            this.request.setAttribute(OBJETO_JSON, null);
        }
    }
}