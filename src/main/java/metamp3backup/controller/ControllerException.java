package metamp3backup.controller;

/**
 * Excepción seguida por los controladores.
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public class ControllerException extends Exception {

    /**
     * Constructor by default
     */
    public ControllerException() {
    }

    /**
     * Shows an error message.
     *
     * @param msg Message error.
     */
    public ControllerException(final String msg) {
        super(msg);
    }
}