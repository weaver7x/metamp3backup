package metamp3backup.controller;

import com.github.sardine.Sardine;
import com.github.sardine.SardineFactory;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;

/**
 * Gestiona las conexiones WebDAV.
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public final class GestorWebDAV {

    /**
     * Suponemos que siempre somos: user: resu
     */
    private final static Sardine sardine = SardineFactory.begin("user", "resu");

    /**
     * @String URI con la ruta absoluta del fichero incluido.
     * @String urlDestino URI absoluta con el fichero incluido al final.
     */
    static void subida(final String uriOrigen, final String uriDestino) {
        System.out.println("\nOrigen: "+uriOrigen+"\nCopiando fichero a " + uriDestino);
        try {
            byte[] data = FileUtils.readFileToByteArray(new File(uriOrigen));
            sardine.put(uriDestino, data);
        } catch (IOException ex) {
            System.out.println("Hubo un error\n" + ex);
        }
    }
}