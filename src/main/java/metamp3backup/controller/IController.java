package metamp3backup.controller;

import javax.servlet.http.HttpServletRequest;

/**
 * Controller handled by Front Controller.
 * Controller interface that all Controller classes will implement.
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public interface IController {
    /**
     * Execution steps.
     * @throws ControllerException If any exception was encountered in the controller.
     */
    public void execute() throws ControllerException;

    /**
     * Initialization process.
     * @param request Current session request.
     */
    public void init(final HttpServletRequest request);
}