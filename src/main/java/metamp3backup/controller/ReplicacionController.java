package metamp3backup.controller;

import static metamp3backup.controller.AbstractController.OBJETO_JSON;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * Controlador del metodo de la API del servidor /Replicacion
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public final class ReplicacionController extends AbstractController {

    @Override
    public void execute() throws ControllerException {
        final String tokenJSON = this.request.getParameter(ConstantsJSON.BACKUP_API_KEY_TOKEN);
        final String idPistaJSON = this.request.getParameter(ConstantsJSON.BACKUP_API_KEY_PISTA_ID);
        final String zonaJSON = this.request.getParameter(ConstantsJSON.BACKUP_API_KEY_ZONA);
        JSONObject respuestaJSON = new JSONObject();
        if (tokenJSON != null && idPistaJSON != null && zonaJSON != null) {
            System.out.println("/Identificacion - Identificación de servidor0");
            respuestaJSON = new JSONObject();
            try {
                final int zonaDestino = Integer.parseInt(zonaJSON);
                final int idPista = Integer.parseInt(idPistaJSON);
                // Seguridad de la aplicación: tokenJSON se da por supuesto que sí.
                if (true) {
                    System.out.println("\t **Gestor de recuperación -> Replicación: prepared.**");
                    System.out.println("\t Subo fichero a: " + zonaToWebDAV(zonaDestino, idPista, ConstantsJSON.EXTENSION_FICHEROS_CON_PUNTO));
                    System.out.println("\t De " + ConstantsJSON.RUTA_INTERNA_WEBDAV + idPista + ConstantsJSON.EXTENSION_FICHEROS_CON_PUNTO);
                    GestorWebDAV.subida(ConstantsJSON.RUTA_INTERNA_WEBDAV + idPista + ConstantsJSON.EXTENSION_FICHEROS_CON_PUNTO,
                            zonaToWebDAV(zonaDestino, idPista, ConstantsJSON.EXTENSION_FICHEROS_CON_PUNTO));
                    respuestaJSON.put(ConstantsJSON.BACKUP_API_KEY_ESTADO, ConstantsJSON.BACKUP_API_KEY_ESTADO_EXITO);
                    System.out.println("\t **Gestor de recuperación -> Replicación: comitted.**");

                } else {
                    respuestaJSON.put(ConstantsJSON.BACKUP_API_KEY_ESTADO, ConstantsJSON.BACKUP_API_KEY_ESTADO_FALLO_NO_LOGIN);
                }

                this.request.setAttribute(OBJETO_JSON, respuestaJSON);
            } catch (final JSONException ex) {
                ex.printStackTrace();
                try {
                    respuestaJSON.put(ConstantsJSON.BACKUP_API_KEY_ESTADO, ConstantsJSON.BACKUP_API_KEY_ESTADO_FALLO);
                } catch (final JSONException ex1) {
                }
            }
        } else {
            this.request.setAttribute(OBJETO_JSON, null);
        }
    }

    private String zonaToWebDAV(final int zona, final int idFichero, final String extensionFicheroConPunto) {
        return "http://pasarela.lab.inf.uva.es:300" + (zona == 1 ? 3 : zona == 2 ? 4 : zona == 3 ? 5 : 0) + "2/webdav/" + idFichero + extensionFicheroConPunto;
    }
}