package metamp3backup.controller;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;

/**
 * Clase que gestiona los ficheros MP3 en cuanto a sus metadatos.
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 1.0-FINAL
 */
public final class AudioTagger {

    /**
     * FLAG que indica que la edicón fue correcta.
     */
    public final static byte EDICION_CORRECTA = 0;
    /**
     * FLAG que indica que la edicón fue incorrecta.
     */
    public final static byte EDICION_INCORRECTA = -1;

    public byte updateFichero(final String ruta, final String nombrePista, final String artista, final String idGenero) {
        try {
            final File archivo = new File(ruta);
            final AudioFile f = AudioFileIO.read(archivo);
            final Tag tagg = (Tag) f.getTag();
            if (nombrePista != null) {
                tagg.setField(FieldKey.TITLE, nombrePista);
            }
            if (artista != null) {
                tagg.setField(FieldKey.ARTIST, artista);
            }
            if (idGenero != null) {
                tagg.setField(FieldKey.GENRE, idGenero);
            }

            AudioFileIO.write(f);
            return EDICION_CORRECTA;
        } catch (final Exception ex) {
            Logger.getLogger(AudioTagger.class.getName()).log(Level.SEVERE, null, ex);
            return EDICION_INCORRECTA;
        }
    }
}