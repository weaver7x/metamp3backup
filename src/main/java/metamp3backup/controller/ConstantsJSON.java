package metamp3backup.controller;

/**
 * Constantes de los protocolos de JSON.
 *
 * @author MetaMP3ASAI1415
 * @since 0.1-ALPHA
 * @version 0.1-ALPHA
 */
public final class ConstantsJSON {

    private ConstantsJSON() {
    }
    /**
     * Clave que representa el identificador del servidor central.
     */
    public final static String BACKUP_API_KEY_ID = "id";
    /**
     * Clave que representa la contraseña para la identificación.
     */
    public final static String BACKUP_API_KEY_PASS = "pass";
    /**
     * Clave que representa el token obtenido tras la identificaicón.
     */
    public final static String BACKUP_API_KEY_TOKEN = "token";
    /**
     * Clave que representa el estado de una peticición.
     */
    public final static String BACKUP_API_KEY_ESTADO = "estado";
    /**
     * Clave que representa el estado de la peticición ha sido exitoso.
     */
    public final static String BACKUP_API_KEY_ESTADO_EXITO = "exito";
    /**
     * Clave que representa el estado de la peticición ha fallado.
     */
    public final static String BACKUP_API_KEY_ESTADO_FALLO = "fallo";
    /**
     * Clave que representa el estado de la peticición ha fallado porque no se
     * estaba logeado.
     */
    public final static String BACKUP_API_KEY_ESTADO_FALLO_NO_LOGIN = "nologin";
    /**
     * Clave que representa la zona.
     */
    public final static String BACKUP_API_KEY_ZONA = "zona";
    /**
     * Clave que representa el identificador de la pista a replicar o editar
     */
    public final static String BACKUP_API_KEY_PISTA_ID = "id";
    /**
     * Clave que representa el identificador del nombre de la canción de la
     * pista a editar.
     */
    public final static String BACKUP_API_KEY_PISTA_NOMBRE_CANCION = "nombreCancion";
    /**
     * Clave que representa el identificador del género del artista de la pista
     * a editar.
     */
    public final static String BACKUP_API_KEY_PISTA_ARTISTA = "artista";
    /**
     * Clave que representa el identificador del género de la pista a editar.
     */
    public final static String BACKUP_API_KEY_PISTA_ID_GENERO = "idGenero";
    /**
     * Ruta interna del servidor del directorio al WebDAV
     */
    public final static String RUTA_INTERNA_WEBDAV = "/var/www/webdav/";
    /**
     * Extensión de los ficheros de la aplicación.
     */
    public final static String EXTENSION_FICHEROS_CON_PUNTO = ".mp3";
    /**
     * Indica que hubo un fallo en el WebDAV.
     */
    public final static byte WEB_DAV_FALLO = -1;
}