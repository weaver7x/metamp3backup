# **MetaMP3Backup** #

Servidor de replicación de la aplicación MetaMP3 de la asignatura ASAI del Máster en Ingeniería Informática de la Universidad de Valladolid.
Lanza un Apache Tomcat embebido.



# ** Autores** #

Cerezo Redondo, Borja

Roda Suárez, Daniel

Tejedor García, Cristian 



# ** Compilación** #

    cd <directorioApp>

    mvn package



# ** Ejecución** #

    cd <directorioApp>

    sh target/bin/webapp

# --> En segundo plano

    nohup sh target/bin/webapp > /dev/null 2>&1 & echo $! > run.pid



# ** JDK** #

Versión 7 del JDK de Java.


# ** Maven (versión 2)** #

    sudo apt-get install maven